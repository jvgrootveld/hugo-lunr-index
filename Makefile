# Parameters
BINARY_NAME=hugo-lunr-index

.PHONY: build
build:
	go build -o bin/${BINARY_NAME} ${BINARY_NAME}.go

.PHONY: run
run:
	go run ${BINARY_NAME}.go