module hugo-lunr-index

require (
	github.com/gernest/front v0.0.0-20181129160812-ed80ca338b88
	github.com/sirupsen/logrus v1.2.0
	github.com/writeas/go-strip-markdown v0.0.0-20180226051418-6e12ec30f699
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
