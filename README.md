# Hugo Lunr Index

Tool to generate a Lunr index for a Hugo content folder.

## Libraries used
- [Logrus](https://github.com/sirupsen/logrus) For logging

## Inspiration
This tool was inspired by [Hugo Golunr](https://github.com/riesinger/hugo-golunr). The did not fully support by needs for my other project, [Knowledge Base](https://gitlab.com/my-knowledge-base/kb-service) \
THis projects adds support for:
- Flags
- Front matter tags
- Extra Logging