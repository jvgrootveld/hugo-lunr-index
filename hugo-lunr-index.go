package main

import (
	"flag"
	"hugo-lunr-index/internal/file"
	"hugo-lunr-index/internal/index"

	log "github.com/sirupsen/logrus"
)

const (
	defaultContentDir           = "./content"
	defaultbaseURL              = "/"
	defaultFrontmatterDelimiter = "---"
	defaultIndexFilePath        = "./index.json"
)

var (
	contentDir           string
	baseURL              string
	frontmatterDelimiter string
	indexFilePath        string
)

func init() {
	// Parse flags
	flag.StringVar(&contentDir, "content", defaultContentDir, "Content directory to create index for")
	flag.StringVar(&baseURL, "baseurl", defaultbaseURL, "Base url to prefix page references")
	flag.StringVar(&frontmatterDelimiter, "frontdelim", defaultFrontmatterDelimiter, "Page frontmater delimiter")
	flag.StringVar(&indexFilePath, "indexfile", defaultIndexFilePath, "The path of the result index file")

	flag.Usage = func() {
		flag.PrintDefaults()
	}

	flag.Parse()
}

func main() {
	documents, err := index.GenerateIndex(contentDir)
	if err != nil {
		log.Fatalf("Error while generating index for '%s'. Error: %v", contentDir, err)
	}

	file.WriteIndexFile(indexFilePath, documents)
}
