package file

import (
	"encoding/json"
	"hugo-lunr-index/internal/lunr"
	"io/ioutil"
	"os"
)

const filePermissions os.FileMode = 0644

func WriteIndexFile(path string, documents []lunr.Document) error {
	bytes, err := json.Marshal(documents)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(path, bytes, filePermissions)
	if err != nil {
		return err
	}

	return nil
}
