package lunr

import (
	"flag"
	"github.com/gernest/front"
	"github.com/writeas/go-strip-markdown"
	"strings"
)

type Document struct {
	Title   string   `json:"name"`
	Href    string   `json:"href"`
	Tags    []string `json:"tags"`
	Content string   `json:"text"`
}

func CreateDocument(path string, fileName string, fileContent string) (Document, error) {
	matter := front.NewMatter()
	matter.Handle(frontmatterDelimiter(), front.YAMLHandler)
	frontMatter, rawBody, err := matter.Parse(strings.NewReader(fileContent))
	if err != nil {
		if err != front.ErrUnknownDelim {
			return Document{}, err
		}

		frontMatter = nil
	}

	return Document{
		Title:   parseTitle(frontMatter, fileName),
		Href:    createHref(path),
		Tags:    parseTags(frontMatter),
		Content: parseContent(rawBody),
	}, nil
}

// Retrieve title from front matter
// Falls back to fileName if not found
func parseTitle(frontMatter map[string]interface{}, fileName string) string {
	if title, exists := frontMatter["title"]; exists {
		return stripmd.Strip(title.(string))
	}

	return fileName
}

// Create href for page referencing
// Strips content dir and markdown extension
//
func createHref(path string) string {
	uri := strings.TrimPrefix(path, "content/") // Absolute path
	uri = strings.TrimSuffix(uri, ".md")        // Remove suffix
	uri = strings.TrimSuffix(uri, "/index")     // Check for index page

	return baseUrl() + uri
}

func parseTags(frontMatter map[string]interface{}) []string {
	if tags, exists := frontMatter["tags"]; exists {

		var parsedTags []string
		for _, tag := range tags.([]interface{}) {
			parsedTags = append(parsedTags, tag.(string))
		}

		return parsedTags
	}

	return []string{}
}

func parseContent(content string) string {
	return stripmd.Strip(content)
}

// Flag configs

func frontmatterDelimiter() string {
	return flag.Lookup("frontdelim").Value.(flag.Getter).Get().(string)
}

func baseUrl() string {
	return flag.Lookup("baseurl").Value.(flag.Getter).Get().(string)
}
