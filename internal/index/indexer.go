package index

import (
	"hugo-lunr-index/internal/lunr"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

const markdownSuffix = ".md"

func GenerateIndex(directory string) ([]lunr.Document, error) {
	var documents []lunr.Document

	err := filepath.Walk(directory, func(path string, info os.FileInfo, err error) error {
		// Bubble up error
		if err != nil {
			return err
		}

		// Skip directories
		if info.IsDir() {
			return nil
		}

		// Check if file is Markdown (ends with .md)
		if !strings.HasSuffix(info.Name(), markdownSuffix) {
			log.Warn("File '%s' ignored, does not have the support extension '%s' ", info.Name(), markdownSuffix)
			return nil
		}

		content, err := fileContent(path)
		if err != nil {
			return err
		}

		document, err := lunr.CreateDocument(path, info.Name(), content)
		if err != nil {
			return err
		}

		documents = append(documents, document)

		return nil
	})
	if err != nil {
		return nil, err
	}

	return documents, nil
}

func fileContent(path string) (string, error) {
	buf, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(buf), nil
}
